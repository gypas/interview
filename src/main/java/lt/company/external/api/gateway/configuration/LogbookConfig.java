package lt.company.external.api.gateway.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.HeaderFilter;
import org.zalando.logbook.HeaderFilters;
import org.zalando.logbook.Sink;
import org.zalando.logbook.json.JsonHttpLogFormatter;
import org.zalando.logbook.logstash.LogstashLogbackSink;

import java.util.List;

@Configuration
public class LogbookConfig {

    private static final List<String> WHITELISTED_HEADERS = List.of("WWW-Authenticate", "X-Real-IP", "X-Forwarded-For");

    @Bean
    public HeaderFilter headerWhitelistFilter() {
        return HeaderFilter.merge(
                HeaderFilters.defaultValue(),
                HeaderFilters.removeHeaders(s -> !WHITELISTED_HEADERS.contains(s)));
    }

    @Bean
    Sink logbookLogstashSink() {
        var formatter = new JsonHttpLogFormatter();
        return new LogstashLogbackSink(formatter);
    }
}
