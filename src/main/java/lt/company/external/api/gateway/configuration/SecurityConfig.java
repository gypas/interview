package lt.company.external.api.gateway.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

import java.util.List;

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    @Value("${gateway.security.whitelist}")
    private List<String> pathWhitelist;

    public static final String ACTUATOR_ENDPOINT = "/actuator/**";
    public static final List<String> SWAGGER_ENDPOINTS = List.of("/swagger-ui/**", "/v2/**", "/swagger-resources/**");

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        var routeSwaggerEndpoints = SWAGGER_ENDPOINTS.stream()
                .map(s -> "/*" + s)
                .toArray(String[]::new);

        http.authorizeExchange()
                .pathMatchers(ACTUATOR_ENDPOINT).permitAll()
                .pathMatchers(SWAGGER_ENDPOINTS.toArray(new String[0])).permitAll()
                .pathMatchers(routeSwaggerEndpoints).permitAll()
                .pathMatchers(pathWhitelist.toArray(new String[0])).permitAll()
                .anyExchange().authenticated().and()
                .csrf().disable()
                .oauth2ResourceServer(ServerHttpSecurity.OAuth2ResourceServerSpec::jwt);

        return http.build();
    }
}