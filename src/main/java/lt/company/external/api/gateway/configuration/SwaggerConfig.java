package lt.company.external.api.gateway.configuration;

import com.fasterxml.classmate.TypeResolver;
import lt.company.external.api.gateway.controller.ControllerLocator;
import lt.company.external.api.gateway.handler.ReactorHandlerMethodResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spring.web.readers.operation.HandlerMethodResolver;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String TITLE = "External API Gateway service";
    private static final String DESCRIPTION = "API Gateway for external request routing";

    @Bean
    public Docket api() {
        return (new Docket(DocumentationType.SWAGGER_2))
                .useDefaultResponseMessages(false)
                .consumes(Set.of("application/json"))
                .produces(Set.of("application/json"))
                .select()
                .apis(RequestHandlerSelectors.basePackage(ControllerLocator.class.getPackageName()))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(this.apiInfo(TITLE, DESCRIPTION));
    }

    @Bean
    public UiConfiguration disableTryItOutButton() {
        return UiConfigurationBuilder.builder()
                .supportedSubmitMethods(UiConfiguration.Constants.NO_SUBMIT_METHODS)
                .build();
    }

    @Primary
    @Bean
    //Unwrap types of reactive objects
    public HandlerMethodResolver reactiveMethodResolver(TypeResolver resolver) {
        return new ReactorHandlerMethodResolver(resolver);
    }

    private ApiInfo apiInfo(String title, String description) {
        return new ApiInfo(title, description, (String) null, (String) null, (Contact) null, (String) null, (String) null, Collections.emptyList());
    }
}
