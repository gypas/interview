package lt.company.external.api.gateway.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.security.Authenticator;
import lt.company.external.api.gateway.service.ApiRouteService;
import lt.company.external.api.gateway.annotation.DefaultErrorResponses;
import lt.company.external.api.gateway.dao.ApiRoute;
import lt.company.external.api.gateway.dto.ApiRouteRequest;
import lt.company.external.api.gateway.dto.ApiRouteResponse;
import lt.company.external.api.gateway.dto.RoutePathRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@RestController
@RequestMapping("/internal/routes")
@RequiredArgsConstructor
//Swagger
@Api(tags = "Private routing API")
@DefaultErrorResponses

public class ApiRouteController {

    private final ApiRouteService apiRouteService;
    private final MapperFunction<ApiRoute, ApiRouteResponse> apiRouteResponseMapperFunction;
    private final Authenticator authenticator;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all routes from database")
    public Flux<ApiRouteResponse> findApiRoutes(@ApiIgnore @AuthenticationPrincipal Jwt jwt) {
        authenticator.validateAdmin(jwt);

        return Flux.fromIterable(apiRouteService.findApiRoutes())
                .map(apiRouteResponseMapperFunction);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create new route")
    public Mono<Void> createApiRoute(@RequestBody @Valid ApiRouteRequest apiRouteRequest,
                                     @ApiIgnore @AuthenticationPrincipal Jwt jwt) {
        authenticator.validateAdmin(jwt);

        return apiRouteService.createApiRoute(apiRouteRequest);
    }

    @PostMapping(path = "/whitelist", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create new route whitelisted path")
    public Mono<Void> createRoutePath(@RequestBody @Valid RoutePathRequest routePathRequest,
                                      @ApiIgnore @AuthenticationPrincipal Jwt jwt) {
        authenticator.validateAdmin(jwt);

        return apiRouteService.createRoutePath(routePathRequest);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete route")
    public Mono<Void> deleteApiRoute(@PathVariable Long id,
                                     @ApiIgnore @AuthenticationPrincipal Jwt jwt) {
        authenticator.validateAdmin(jwt);

        return apiRouteService.deleteApiRoute(id);
    }

    @DeleteMapping(path = "/whitelist/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete whitelisted route path")
    public Mono<Void> deleteRoutePath(@PathVariable Long id,
                                      @ApiIgnore @AuthenticationPrincipal Jwt jwt) {
        authenticator.validateAdmin(jwt);

        return apiRouteService.deleteRoutePath(id);
    }

    @ApiOperation(value = "Refresh routes from database to API Gateway")
    @GetMapping(path = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Void> refresh(@ApiIgnore @AuthenticationPrincipal Jwt jwt) {
        authenticator.validateAdmin(jwt);

        return apiRouteService.refresh();
    }
}
