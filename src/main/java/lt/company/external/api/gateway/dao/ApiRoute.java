package lt.company.external.api.gateway.dao;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "external_routes", schema = "api_gateway")
@Getter
@Setter
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class ApiRoute {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  private String path;
  private String url;
  private String serviceName;

  @OneToMany(mappedBy = "apiRoute", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private List<RoutePath> routePaths = new ArrayList<>();
}
