package lt.company.external.api.gateway.dao;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "route_paths", schema = "api_gateway")
@Getter
@Setter
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class RoutePath {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String path;

  @ManyToOne
  @JoinColumn(name = "ROUTE_ID")
  @ToString.Exclude
  private ApiRoute apiRoute;
}
