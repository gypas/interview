package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.dao.ApiRoute;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ApiRouteMapper implements MapperFunction<ApiRouteRequest, ApiRoute> {

    @Override
    public ApiRoute apply(ApiRouteRequest apiRouteRequest) {
        var apiRoute = new ApiRoute();

        apiRoute.setPath(apiRouteRequest.getPath());
        apiRoute.setUrl(apiRouteRequest.getUrl());
        apiRoute.setServiceName(apiRouteRequest.getServiceName());

        if (apiRouteRequest.getPathWhitelist() != null) {
            apiRoute.setRoutePaths(
                    apiRouteRequest.getPathWhitelist()
                            .stream()
                            .map(path -> RoutePath.builder().path(path).apiRoute(apiRoute).build())
                            .collect(Collectors.toList()));
        }

        return apiRoute;
    }
}
