package lt.company.external.api.gateway.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@ToString
@RequiredArgsConstructor
public class ApiRouteRequest {

    @NotBlank
    private final String path;

    private final String url;
    private final String serviceName;
    private final List<String> pathWhitelist;
}