package lt.company.external.api.gateway.dto;

import lombok.*;

import java.util.List;

@Builder
@ToString
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiRouteResponse {

    private final Long id;
    private final String path;
    private final String url;
    private final String serviceName;
    private final List<RoutePathResponse> pathWhitelist;
}
