package lt.company.external.api.gateway.dto;

import lombok.RequiredArgsConstructor;
import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.dao.ApiRoute;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ApiRouteResponseMapper implements MapperFunction<ApiRoute, ApiRouteResponse> {

    private final MapperFunction<RoutePath, RoutePathResponse> routePathResponseMapper;

    @Override
    public ApiRouteResponse apply(ApiRoute apiRoute) {
        return ApiRouteResponse.builder()
                .id(apiRoute.getId())
                .path(apiRoute.getPath())
                .url(apiRoute.getUrl())
                .serviceName(apiRoute.getServiceName())
                .pathWhitelist(apiRoute.getRoutePaths().stream().map(routePathResponseMapper).collect(Collectors.toList()))
                .build();
    }
}
