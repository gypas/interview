package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.dao.ApiRoute;
import org.springframework.stereotype.Component;

@Component
public class RoutePathMapper implements MapperFunction<RoutePathRequest, RoutePath> {

    @Override
    public RoutePath apply(RoutePathRequest routePathRequest) {
        return RoutePath.builder()
                .apiRoute(ApiRoute.builder().id(routePathRequest.getRouteId()).build())
                .path(routePathRequest.getPath())
                .build();
    }
}