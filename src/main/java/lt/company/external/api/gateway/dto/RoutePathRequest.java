package lt.company.external.api.gateway.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public class RoutePathRequest {

    private final Long routeId;
    private final String path;
}
