package lt.company.external.api.gateway.dto;

import lombok.*;

@Builder
@ToString
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class RoutePathResponse {

    private final Long id;
    private final String path;
}
