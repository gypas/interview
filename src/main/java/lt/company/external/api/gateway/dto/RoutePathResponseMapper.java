package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.mapper.MapperFunction;
import org.springframework.stereotype.Component;

@Component
public class RoutePathResponseMapper implements MapperFunction<RoutePath, RoutePathResponse> {

    @Override
    public RoutePathResponse apply(RoutePath routePath) {
        return RoutePathResponse.builder()
                .id(routePath.getId())
                .path(routePath.getPath())
                .build();
    }
}
