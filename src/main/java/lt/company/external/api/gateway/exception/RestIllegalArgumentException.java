package lt.company.external.api.gateway.exception;

@SuppressWarnings("unused")
public final class RestIllegalArgumentException extends RuntimeException {

    public RestIllegalArgumentException(final String message) {
        super(message);
    }
}