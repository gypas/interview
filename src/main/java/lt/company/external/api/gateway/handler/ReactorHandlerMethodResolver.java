package lt.company.external.api.gateway.handler;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.classmate.types.ResolvedArrayType;
import org.springframework.web.method.HandlerMethod;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import springfox.documentation.spring.web.readers.operation.HandlerMethodResolver;

@SuppressWarnings("unused") //todo pasalinti po CZ-297
public class ReactorHandlerMethodResolver extends HandlerMethodResolver {

    public ReactorHandlerMethodResolver(TypeResolver typeResolver) {
        super(typeResolver);
    }

    @Override
    public ResolvedType methodReturnType(HandlerMethod handlerMethod) {
        var retType = super.methodReturnType(handlerMethod);

        //Flux unwrap
        if (retType.getErasedType() == Flux.class) {
            var type = retType.getTypeBindings().getBoundType(0);
            retType = new ResolvedArrayType(type.getErasedType(), type.getTypeBindings(), type);
        }

        //Mono unwrap
        if (retType.getErasedType() == Mono.class) {
            retType = retType.getTypeBindings().getBoundType(0);
        }

        return retType;
    }
}