package lt.company.external.api.gateway.mapper;

import java.util.function.Function;

public interface MapperFunction<T, R> extends Function<T, R> {

    @Override
    R apply(T t);
}
