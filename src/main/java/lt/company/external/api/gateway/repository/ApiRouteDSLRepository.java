package lt.company.external.api.gateway.repository;

import lt.company.external.api.gateway.dao.ApiRoute;

import java.util.List;
import java.util.Optional;

public interface ApiRouteDSLRepository {

    List<ApiRoute> findAllRoutes();

    Optional<ApiRoute> findByRouteId(Long id);
}
