package lt.company.external.api.gateway.repository;

import com.querydsl.jpa.impl.JPAQuery;
import lombok.RequiredArgsConstructor;
import lt.company.external.api.gateway.dao.ApiRoute;
import lt.company.external.api.gateway.dao.QApiRoute;
import lt.company.external.api.gateway.dao.QRoutePath;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class ApiRouteDSLRepositoryImpl implements ApiRouteDSLRepository {

     @PersistenceContext
     private final EntityManager em;

    public List<ApiRoute> findAllRoutes() {
         var apiRoute = QApiRoute.apiRoute;
         var routePath = QRoutePath.routePath;

         return new JPAQuery<ApiRoute>(em)
                 .from(apiRoute)
                 .leftJoin(apiRoute.routePaths, routePath).fetchJoin()
                 .distinct()
                 .fetch();
    }

    public Optional<ApiRoute> findByRouteId(Long id) {
        var apiRoute = QApiRoute.apiRoute;

        return Optional.ofNullable(
                new JPAQuery<ApiRoute>(em)
                        .from(apiRoute)
                        .where(apiRoute.id.eq(id))
                        .fetchOne());
    }
}
