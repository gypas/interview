package lt.company.external.api.gateway.repository;

import lt.company.external.api.gateway.dao.ApiRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiRouteRepository extends JpaRepository<ApiRoute, Long>, ApiRouteDSLRepository {
}
