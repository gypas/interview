package lt.company.external.api.gateway.repository;

import lt.company.external.api.gateway.dao.RoutePath;

import java.util.Optional;

public interface RoutePathDSLRepository {

    Optional<RoutePath> findByPathId(Long id);

    void deleteByPathId(Long id);
}
