package lt.company.external.api.gateway.repository;

import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.RequiredArgsConstructor;
import lt.company.external.api.gateway.dao.QApiRoute;
import lt.company.external.api.gateway.dao.QRoutePath;
import lt.company.external.api.gateway.dao.RoutePath;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@RequiredArgsConstructor
public class RoutePathDSLRepositoryImpl implements RoutePathDSLRepository {

    @PersistenceContext
    private final EntityManager em;

    public Optional<RoutePath> findByPathId(Long id) {
        var routePath = QRoutePath.routePath;
        var apiRoute = QApiRoute.apiRoute;

        return Optional.ofNullable(
                new JPAQuery<RoutePath>(em)
                        .from(routePath)
                        .join(routePath.apiRoute, apiRoute).fetchJoin()
                        .where(routePath.id.eq(id))
                        .fetchOne());
    }

    @Transactional
    public void deleteByPathId(Long id) {
        var routePath = QRoutePath.routePath;

        new JPADeleteClause(em, routePath).where(routePath.id.eq(id)).execute();
    }
}
