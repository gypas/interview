package lt.company.external.api.gateway.repository;

import lt.company.external.api.gateway.dao.RoutePath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoutePathRepository extends JpaRepository<RoutePath, Long>, RoutePathDSLRepository {
}
