package lt.company.external.api.gateway.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class Authenticator {
    private static final String ADMIN_SCOPE = "api_gw_admin";

    public void validateAdmin(Jwt jwt) {
        var scopes = jwt.getClaimAsStringList("scope");

        if (!scopes.contains(ADMIN_SCOPE)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }
}
