package lt.company.external.api.gateway.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lt.company.external.api.gateway.dao.ApiRoute;
import lt.company.external.api.gateway.dao.RoutePath;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class ApiPathRouteLocatorImpl implements RouteLocator {

    private final ApiRouteService apiRouteService;
    private final RouteLocatorBuilder routeLocatorBuilder;
    private final DiscoveryClient discoveryClient;

    @Override
    public Flux<Route> getRoutes() {
        var routesBuilder = routeLocatorBuilder.routes();

        apiRouteService.findApiRoutes().stream()
                .filter(this::routeIsValid)
                .forEach(apiRoute -> {
                    if (apiRoute.getRoutePaths().isEmpty()) {
                        routesBuilder.route(predicateSpec -> createRoute(apiRoute, "/**", predicateSpec));
                    }

                    apiRoute.getRoutePaths().stream()
                            .map(RoutePath::getPath)
                            .forEach(path -> routesBuilder.route(predicateSpec -> createRoute(apiRoute, path, predicateSpec)));
                });

        return routesBuilder.build().getRoutes();
    }

    private boolean routeIsValid(ApiRoute apiRoute) {
        // Kubernetes route validation
        if (apiRoute.getServiceName() != null) {
            var optionalServiceName = findKubernetesServiceName(apiRoute.getServiceName());

            if (optionalServiceName.isEmpty()) {
                log.error("Service not found in kubernetes. ApiRoute: {}", apiRoute);
                return false;
            }
            if (discoveryClient.getInstances(optionalServiceName.get()).isEmpty()) {
                log.error("No instances of service found in kubernetes. ApiRoute: {}", apiRoute);
                return false;
            }
        // Url route validation
        } else {
            if (apiRoute.getUrl() == null) {
                log.error("No service name or url provided. ApiRoute: {}", apiRoute);
                return false;
            }
            if (!UrlValidator.getInstance().isValid(apiRoute.getUrl())) {
                log.error("Provided url is invalid. ApiRoute: {}", apiRoute);
                return false;
            }
        }
        return true;
    }

    private Buildable<Route> createRoute(ApiRoute apiRoute, String path, PredicateSpec predicateSpec) {
        if (apiRoute.getServiceName() != null) {
            return createKubernetesRoute(apiRoute, path, predicateSpec);
        } else {
            return createUrlRoute(apiRoute, path, predicateSpec);
        }
    }

    private Buildable<Route> createUrlRoute(ApiRoute apiRoute, String routePath, PredicateSpec predicateSpec) {
        var path = apiRoute.getPath() + routePath;

        return setPredicateSpec(predicateSpec, path, apiRoute.getUrl());
    }

    private Buildable<Route> createKubernetesRoute(ApiRoute apiRoute, String routePath, PredicateSpec predicateSpec) {
        var serviceName = findKubernetesServiceName(apiRoute.getServiceName()).orElseThrow(() -> new RuntimeException("Unexpected exception"));

        return discoveryClient.getInstances(serviceName)
                .stream()
                .map(instance -> {
                    var path = apiRoute.getPath() + routePath;
                    var url = "http://" + instance.getServiceId();

                    return setPredicateSpec(predicateSpec, path, url);
                }).findFirst()
                .orElseThrow(() -> new RuntimeException("Unexpected exception"));
    }

    private Buildable<Route> setPredicateSpec(PredicateSpec predicateSpec, String path, String url) {
        log.info("Routing " + path + " to " + url);

        var booleanSpec = predicateSpec.path(path);
        booleanSpec.filters(f -> f.rewritePath("/(?<base>.*?)/(?<segment>.*)", "/$\\{segment}"));

        return booleanSpec.uri(url);
    }

    private Optional<String> findKubernetesServiceName(String serviceName) {
        return discoveryClient.getServices()
                .stream()
                .filter(s -> s.startsWith(serviceName))
                .findFirst();
    }
}
