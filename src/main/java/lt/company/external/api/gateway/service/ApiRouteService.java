package lt.company.external.api.gateway.service;

import lt.company.external.api.gateway.dao.ApiRoute;
import lt.company.external.api.gateway.dto.ApiRouteRequest;
import lt.company.external.api.gateway.dto.RoutePathRequest;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ApiRouteService {

    List<ApiRoute> findApiRoutes();

    Mono<Void> createApiRoute(ApiRouteRequest apiRouteRequest);

    Mono<Void> createRoutePath(RoutePathRequest routePathRequest);

    Mono<Void> deleteApiRoute(Long id);

    Mono<Void> deleteRoutePath(Long id);

    Mono<Void> refresh();
}
