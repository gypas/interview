package lt.company.external.api.gateway.service;

import lombok.RequiredArgsConstructor;
import lt.company.external.api.gateway.dao.ApiRoute;
import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.dto.RoutePathRequest;
import lt.company.external.api.gateway.exception.RestIllegalArgumentException;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.repository.ApiRouteRepository;
import lt.company.external.api.gateway.repository.RoutePathRepository;
import lt.company.external.api.gateway.dto.ApiRouteRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ApiRouteServiceImpl implements ApiRouteService {

    private final ApiRouteRepository apiRouteRepository;
    private final RoutePathRepository routePathRepository;
    private final GatewayRouteService gatewayRouteService;
    private final MapperFunction<ApiRouteRequest, ApiRoute> apiRouteMapperFunction;
    private final MapperFunction<RoutePathRequest, RoutePath> routePathMapperFunction;

    @Override
    public List<ApiRoute> findApiRoutes() {
        return apiRouteRepository.findAllRoutes();
    }

    @Override
    public Mono<Void> createApiRoute(ApiRouteRequest apiRouteRequest) {

        return Mono.fromSupplier(() -> apiRouteRepository.save(apiRouteMapperFunction.apply(apiRouteRequest)))
                .doOnSuccess(obj -> gatewayRouteService.refreshRoutes())
                .then();
    }

    @Override
    public Mono<Void> createRoutePath(RoutePathRequest routePathRequest) {
        return Mono.fromSupplier(() -> routePathRepository.save(routePathMapperFunction.apply(routePathRequest)))
                .doOnSuccess(obj -> gatewayRouteService.refreshRoutes())
                .then();
    }

    @Override
    public Mono<Void> deleteApiRoute(Long id) {
        return findAndValidateApiRoute(id)
                .doOnSuccess(obj -> {
                    apiRouteRepository.deleteById(id);
                    gatewayRouteService.refreshRoutes();
                }).then();
    }

    @Override
    public Mono<Void> deleteRoutePath(Long id) {
        return findAndValidateRoutePath(id)
                .doOnSuccess(obj -> {
                    routePathRepository.deleteByPathId(id);
                    gatewayRouteService.refreshRoutes();
                }).then();
    }

    @Override
    public Mono<Void> refresh() {
        gatewayRouteService.refreshRoutes();

        return Mono.empty();
    }

    private Mono<ApiRoute> findAndValidateApiRoute(Long id) {
        return Mono.fromSupplier(() -> apiRouteRepository.findByRouteId(id).orElseThrow(() -> new RestIllegalArgumentException("Api route not found. Api route id: " + id)));
    }

    private Mono<RoutePath> findAndValidateRoutePath(Long id) {
        return Mono.fromSupplier(() -> routePathRepository.findByPathId(id).orElseThrow(() -> new RestIllegalArgumentException("Route path not found. Route path id: " + id)));
    }
}
