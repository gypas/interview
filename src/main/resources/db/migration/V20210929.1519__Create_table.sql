create table external_routes(
    id number generated as identity not null,
    path varchar2(255) not null,
    service_name varchar2(255),
    url varchar2(255),
    primary key(id)
);