create table route_paths(
    id number generated as identity not null,
    route_id number null,
    path varchar2(255) not null,
    primary key(id),
    constraint fk_external_routes foreign key (route_id) references external_routes(id)
);