package lt.company.external.api.gateway.controller;

import lt.company.external.api.gateway.security.Authenticator;
import lt.company.external.api.gateway.service.ApiRouteService;
import lt.company.external.api.gateway.dto.ApiRouteRequest;
import lt.company.external.api.gateway.dto.ApiRouteResponseMapper;
import lt.company.external.api.gateway.dto.RoutePathRequest;
import lt.company.external.api.gateway.dto.RoutePathResponseMapper;
import lt.company.external.api.gateway.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;


@WebFluxTest(value = ApiRouteController.class)
@ActiveProfiles("local")
@ContextConfiguration(classes = {ApiRouteController.class, ApiRouteResponseMapper.class, RoutePathResponseMapper.class})
@ExtendWith(SpringExtension.class)
class ApiRouteControllerTest {

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private ApiRouteService apiRouteService;

    @MockBean
    private Authenticator authenticator;

    @BeforeEach
    void init() {
        Mockito.doNothing().when(authenticator).validateAdmin(Mockito.any());
    }

    @WithMockUser
    @Test
    void findApiRoutes_routesFound_true() {
        Mockito.when(apiRouteService.findApiRoutes()).thenReturn(TestUtils.testApiRoutes());

        webClient.mutateWith(csrf())
                .get()
                .uri("/internal/routes")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .json("[{\"id\":1,\"path\":\"/test\",\"url\":\"http://127.0.0.1:8081\",\"serviceName\":null},{\"id\":2,\"path\":\"/service\",\"url\":\"http://127.0.0.1:8082\",\"serviceName\":null}]");
    }

    @WithMockUser
    @Test
    void createApiRoute_routeCreated_true() {
        var apiRouteRequest = new ApiRouteRequest(
                "/test",
                "http://127.0.0.1:8081/test",
                null,
                List.of(
                        "/test/do/something",
                        "/service/**"));

        Mockito.when(apiRouteService.createApiRoute(apiRouteRequest)).thenReturn(Mono.empty());

        webClient.mutateWith(csrf())
                .post()
                .uri("/internal/routes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(apiRouteRequest))
                .exchange()
                .expectStatus().isCreated();
    }

    @WithMockUser
    @Test
    void createRoutePath_routePathCreated_true() {
        var routePathRequest = new RoutePathRequest(1L, "/test/**");

        Mockito.when(apiRouteService.createRoutePath(routePathRequest)).thenReturn(Mono.empty());

        webClient.mutateWith(csrf())
                .post()
                .uri("/internal/routes/whitelist")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(routePathRequest))
                .exchange()
                .expectStatus().isCreated();
    }

    @WithMockUser
    @Test
    void deleteApiRoute_routeDeleted_true() {
        Mockito.when(apiRouteService.deleteApiRoute(Mockito.anyLong())).thenReturn(Mono.empty());

        webClient.mutateWith(csrf())
                .delete()
                .uri("/internal/routes/1")
                .exchange()
                .expectStatus().isOk();
    }

    @WithMockUser
    @Test
    void deleteRoutePath_routePathDeleted_true() {
        Mockito.when(apiRouteService.deleteRoutePath(Mockito.anyLong())).thenReturn(Mono.empty());

        webClient.mutateWith(csrf())
                .delete()
                .uri("/internal/routes/whitelist/1")
                .exchange()
                .expectStatus().isOk();
    }

    @WithMockUser
    @Test
    void refreshRotes_routesRefreshed_true() {
        Mockito.when(apiRouteService.refresh()).thenReturn(Mono.empty());

        webClient.mutateWith(csrf())
                .get()
                .uri("/internal/routes/refresh")
                .exchange()
                .expectStatus().isOk();
    }
}