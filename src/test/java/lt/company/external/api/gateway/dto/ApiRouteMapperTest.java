package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.dao.ApiRoute;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class ApiRouteMapperTest {

    private final ApiRouteMapper mapper = new ApiRouteMapper();

    @Test
    void apply_routeMapped_true() {
        var path = "/test";
        var url = "http://127.0.0.1:8090";
        var routePath1 = "/test/**";
        var routePath2 = "test2/something";
        var routePathList = List.of(routePath1, routePath2);
        var routeRequest = new ApiRouteRequest(path, url, null, routePathList);

        var routeExpected = new ApiRoute();
        routeExpected.setPath(path);
        routeExpected.setUrl(url);
        routeExpected.setRoutePaths(List.of(
                RoutePath.builder().apiRoute(routeExpected).path(routePath1).build(),
                RoutePath.builder().apiRoute(routeExpected).path(routePath2).build()
        ));

        Assertions.assertEquals(routeExpected.toString(), mapper.apply(routeRequest).toString());
    }

    @Test
    void applyNoRoutePath_routeMapped_true() {
        var path = "/test";
        var url = "http://127.0.0.1:8090";
        var routeRequest = new ApiRouteRequest(path, url, null, null);

        var routeExpected = new ApiRoute();
        routeExpected.setPath(path);
        routeExpected.setUrl(url);

        Assertions.assertEquals(routeExpected.toString(), mapper.apply(routeRequest).toString());
    }
}