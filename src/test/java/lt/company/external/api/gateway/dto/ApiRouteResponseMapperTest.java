package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.dao.ApiRoute;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class ApiRouteResponseMapperTest {

    private ApiRouteResponseMapper mapper;

    @Mock
    private MapperFunction<RoutePath, RoutePathResponse> routePathResponseMapperFunctionMock;

    @BeforeEach
    void before() {
        mapper = new ApiRouteResponseMapper(routePathResponseMapperFunctionMock);
    }

    @Test
    void apply_routeMapped_true() {
        long id = 1L;
        var path = "/test";
        var url = "http://127.0.0.1:8090";
        long path1Id = 1L;
        var routePath1 = "/test/**";
        long path2Id = 2L;
        var routePath2 = "test2/something";

        var route = new ApiRoute();
        route.setId(id);
        route.setPath(path);
        route.setUrl(url);
        route.setRoutePaths(List.of(
                RoutePath.builder().id(path1Id).apiRoute(route).path(routePath1).build(),
                RoutePath.builder().id(path2Id).apiRoute(route).path(routePath2).build()
        ));

        Mockito.when(routePathResponseMapperFunctionMock.apply(Mockito.any()))
                .thenReturn(RoutePathResponse.builder().id(path1Id).path(routePath1).build())
                .thenReturn(RoutePathResponse.builder().id(path2Id).path(routePath2).build());

        var routeResponseExpected = ApiRouteResponse.builder()
                .id(id)
                .path(path)
                .url(url)
                .pathWhitelist(List.of(
                        RoutePathResponse.builder().id(path1Id).path(routePath1).build(),
                        RoutePathResponse.builder().id(path2Id).path(routePath2).build()))
                .build();

        Assertions.assertEquals(routeResponseExpected.toString(), mapper.apply(route).toString());

        Mockito.verify(routePathResponseMapperFunctionMock, Mockito.times(2)).apply(Mockito.any());
    }
}