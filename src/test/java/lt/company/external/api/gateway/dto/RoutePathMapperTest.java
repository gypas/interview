package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.dao.ApiRoute;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RoutePathMapperTest {

    private final RoutePathMapper mapper = new RoutePathMapper();

    @Test
    void apply_routePathMapped_true() {
        long id = 1L;
        var path = "/test";
        var routePathRequest = new RoutePathRequest(id, path);

        var routePathExpected = RoutePath.builder()
                .apiRoute(ApiRoute.builder().id(id).build())
                .path(path)
                .build();

        Assertions.assertEquals(routePathExpected.toString(), mapper.apply(routePathRequest).toString());
    }
}