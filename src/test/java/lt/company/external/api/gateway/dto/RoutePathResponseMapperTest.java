package lt.company.external.api.gateway.dto;

import lt.company.external.api.gateway.dao.RoutePath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RoutePathResponseMapperTest {

    private final RoutePathResponseMapper mapper = new RoutePathResponseMapper();

    @Test
    void apply_routePathResponseMapped_true() {
        long id = 1L;
        var path = "/test";
        var routePath = RoutePath.builder()
                .id(id)
                .path(path)
                .build();

        var routePathResponseExpected = RoutePathResponse.builder()
                .id(id)
                .path(path)
                .build();

        Assertions.assertEquals(routePathResponseExpected.toString(), mapper.apply(routePath).toString());
    }
}