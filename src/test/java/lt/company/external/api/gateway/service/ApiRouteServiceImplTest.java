package lt.company.external.api.gateway.service;

import lt.company.external.api.gateway.dao.RoutePath;
import lt.company.external.api.gateway.exception.RestIllegalArgumentException;
import lt.company.external.api.gateway.mapper.MapperFunction;
import lt.company.external.api.gateway.repository.ApiRouteRepository;
import lt.company.external.api.gateway.repository.RoutePathRepository;
import lt.company.external.api.gateway.dao.ApiRoute;
import lt.company.external.api.gateway.dto.ApiRouteRequest;
import lt.company.external.api.gateway.dto.RoutePathRequest;
import lt.company.external.api.gateway.utils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class ApiRouteServiceImplTest {

    private ApiRouteService apiRouteService;

    @Mock
    private ApiRouteRepository apiRouteRepositoryMock;

    @Mock
    private RoutePathRepository routePathRepositoryMock;

    @Mock
    private GatewayRouteService gatewayRouteServiceMock;

    @Mock
    private MapperFunction<ApiRouteRequest, ApiRoute> apiRouteMapperFunctionMock;

    @Mock
    private MapperFunction<RoutePathRequest, RoutePath> routePathMapperFunctionMock;

    @BeforeEach
    void before() {
        apiRouteService = new ApiRouteServiceImpl(apiRouteRepositoryMock, routePathRepositoryMock, gatewayRouteServiceMock, apiRouteMapperFunctionMock, routePathMapperFunctionMock);
    }

    @Test
    void findApiRoutes_routesFound_true() {
        var routes = TestUtils.testApiRoutes();
        Mockito.when(apiRouteRepositoryMock.findAllRoutes()).thenReturn(routes);

        StepVerifier.create(Flux.fromIterable(apiRouteService.findApiRoutes()))
                .expectNext(routes.get(0))
                .expectNext(routes.get(1))
                .verifyComplete();

        Mockito.verify(apiRouteRepositoryMock).findAllRoutes();
    }

    @Test
    void createRoute_routeCreated_true() {
        var requestMock = Mockito.mock(ApiRouteRequest.class);
        var apiRouteMock = Mockito.mock(ApiRoute.class);

        Mockito.when(apiRouteMapperFunctionMock.apply(requestMock)).thenReturn(apiRouteMock);
        Mockito.when(apiRouteRepositoryMock.save(apiRouteMock)).thenReturn(apiRouteMock);
        Mockito.doNothing().when(gatewayRouteServiceMock).refreshRoutes();

        StepVerifier.create(apiRouteService.createApiRoute(requestMock)).verifyComplete();

        Mockito.verify(apiRouteMapperFunctionMock).apply(requestMock);
        Mockito.verify(apiRouteRepositoryMock).save(apiRouteMock);
        Mockito.verify(gatewayRouteServiceMock).refreshRoutes();
    }

    @Test
    void createRoutePath_routePathCreated_true() {
        var requestMock = Mockito.mock(RoutePathRequest.class);
        var routePathMock = Mockito.mock(RoutePath.class);

        Mockito.when(routePathMapperFunctionMock.apply(requestMock)).thenReturn(routePathMock);
        Mockito.when(routePathRepositoryMock.save(routePathMock)).thenReturn(routePathMock);
        Mockito.doNothing().when(gatewayRouteServiceMock).refreshRoutes();

        StepVerifier.create(apiRouteService.createRoutePath(requestMock)).verifyComplete();

        Mockito.verify(routePathMapperFunctionMock).apply(requestMock);
        Mockito.verify(routePathRepositoryMock).save(routePathMock);
        Mockito.verify(gatewayRouteServiceMock).refreshRoutes();
    }

    @Test
    void deleteApiRoute_routeDeleted_true() {
        long id = 1L;
        var apiRouteMock = Mockito.mock(ApiRoute.class);

        Mockito.when(apiRouteRepositoryMock.findByRouteId(id)).thenReturn(Optional.of(apiRouteMock));
        Mockito.doNothing().when(apiRouteRepositoryMock).deleteById(id);
        Mockito.doNothing().when(gatewayRouteServiceMock).refreshRoutes();

        StepVerifier.create(apiRouteService.deleteApiRoute(id)).verifyComplete();

        Mockito.verify(apiRouteRepositoryMock).findByRouteId(id);
        Mockito.verify(apiRouteRepositoryMock).deleteById(id);
        Mockito.verify(gatewayRouteServiceMock).refreshRoutes();
    }

    @Test
    void deleteRoutePath_routePathDeleted_true() {
        long id = 1L;
        var routePathMock = Mockito.mock(RoutePath.class);

        Mockito.when(routePathRepositoryMock.findByPathId(id)).thenReturn(Optional.of(routePathMock));
        Mockito.doNothing().when(routePathRepositoryMock).deleteByPathId(id);
        Mockito.doNothing().when(gatewayRouteServiceMock).refreshRoutes();

        StepVerifier.create(apiRouteService.deleteRoutePath(id)).verifyComplete();

        Mockito.verify(routePathRepositoryMock).findByPathId(id);
        Mockito.verify(routePathRepositoryMock).deleteByPathId(id);
        Mockito.verify(gatewayRouteServiceMock).refreshRoutes();
    }

    @Test
    void deleteApiRoute_routeNotFound_true() {
        long id = 1L;

        Mockito.when(apiRouteRepositoryMock.findByRouteId(id)).thenReturn(Optional.empty());

        StepVerifier.create(apiRouteService.deleteApiRoute(id))
                .expectError(RestIllegalArgumentException.class)
                .verify();

        Mockito.verify(apiRouteRepositoryMock).findByRouteId(id);
    }

    @Test
    void refreshApiRoutes_routesRefreshed_true() {
        Mockito.doNothing().when(gatewayRouteServiceMock).refreshRoutes();

        StepVerifier.create(apiRouteService.refresh())
                .verifyComplete();

        Mockito.verify(gatewayRouteServiceMock).refreshRoutes();
    }
}