package lt.company.external.api.gateway.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

@ExtendWith(MockitoExtension.class)
class GatewayRouteServiceTest {

    private GatewayRouteService gatewayRouteService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @BeforeEach
    void before() {
        gatewayRouteService = new GatewayRouteServiceImpl(applicationEventPublisher);
    }

    @Test
    void refreshRoutes_true() {
        Mockito.doNothing().when(applicationEventPublisher).publishEvent(Mockito.any());

        Assertions.assertDoesNotThrow(() -> gatewayRouteService.refreshRoutes());
    }
}