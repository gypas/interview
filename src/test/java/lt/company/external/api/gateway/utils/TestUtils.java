package lt.company.external.api.gateway.utils;

import lt.company.external.api.gateway.dao.ApiRoute;

import java.util.List;

public class TestUtils {

    public static List<ApiRoute> testApiRoutes() {
        return List.of(
                createApiRoute(1L,"http://127.0.0.1:8081", "/test"),
                createApiRoute(2L, "http://127.0.0.1:8082", "/service"));
    }

    private static ApiRoute createApiRoute(Long id, String url, String path) {
        var route = new ApiRoute();

        route.setId(id);
        route.setUrl(url);
        route.setPath(path);

        return route;
    }
}
